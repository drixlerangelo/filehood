<?php

namespace App\Http\Controllers;

use App\Entry;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    public function username()
    {
        return 'username';
    }

    public function login()
    {
        request()->validate([
            'username' => 'required|string|min:1',
            'password' => 'required|string|min:1'
        ]);
        
        if (Auth::attempt(request()->only(['username', 'password'])) === true) {
            return redirect()->route('middleman');
        }
    }

    public function index()
    {
        if (Auth::user() === null) {
            return view('welcome');
        }

        return view('home');
    }

    public function logout()
    {
        if (Auth::user() !== null) {
            Auth::logout();
        }

        return redirect()->route('index');
    }

    public function middleman()
    {
        return view('middleman');
    }

    public function keepkey()
    {
        session(['filekey' => request('key')]);

        return redirect()->route('index');
    }
}
