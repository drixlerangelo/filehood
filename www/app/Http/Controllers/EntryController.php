<?php

namespace App\Http\Controllers;

use Illuminate\Encryption\Encrypter;
use App\Entry;
use DB;

class EntryController extends Controller
{
    private $entries = [];
    private $savedno = 0;

    public function place()
    {
        $oneToOne = request()->input('oneToOne');
        $oneToMany = request()->input('oneToMany');

        $validOtO = is_array($oneToOne) && count($oneToOne) === 2;
        $validOtM = is_array($oneToMany) && count($oneToMany) > 0;

        if (!$validOtO && !$validOtM) {
            return redirect()->route('index');
        }

        if ($validOtO) {
            $this->encryptOtO($oneToOne);
        }

        if ($validOtM) {
            $this->encryptOtM($oneToMany);
        }

        $this->saveEntries();

        return redirect()->route('index');
    }

    public function anonymouse()
    {
        $oneToOne = request()->input('oneToOne');
        $realname = (is_bool($realname = request('realname'))) ? $realname : true;
        $validOtO = is_array($oneToOne) && count($oneToOne) === 2;

        if ($validOtO) {
            $this->encryptOtO($oneToOne, false);
        }

        $this->saveEntries();


        echo json_encode([
            'success' => true,
            'message' => $this->savedno . ' entries saved.'
        ]);
    }

    private function saveEntries()
    {
        foreach ($this->entries as $entry) {
            try {
                if ($this->seeIfDuplicate($entry) === false) {
                    $entry['filename'] = $this->encFile($entry['filename']);
                    $entry['password'] = $this->encFile($entry['password']);
                    (new Entry($entry))->save();
                    $this->savedno++;
                }
            } catch (\RuntimeException $re) {
                echo json_encode([
                    'status' => false,
                    'message' => 'invalid key'
                ]);
                exit;
            }
        }
    }

    private function seeIfDuplicate(array $entry) : bool
    {
        foreach ($this->find($entry['filename']) as $result) {
            if ($entry['filename'] === $result['filename'] && $entry['password'] === $result['password']) {
                return true;
            }
        }
        return false;
    }

    private function encryptOtO($oneToOne, $realname = true)
    {
        foreach ($oneToOne['file'] as $iKey => $file) {
            $pass = $oneToOne['pass'][$iKey];

            $file = (is_string($file)) ? trim($file) : '';
            $pass = (is_string($pass)) ? trim($pass) : '';

            $validPass = strlen($pass) > 0;
            $validFile = strlen($file) > 0;

            if ($validFile && $validPass) {
                $this->entries[] = [
                    'filename' => $file,
                    'password' => $pass,
                    'realname' => $realname
                ];
            }

        }
    }

    private function encryptOtM($oneToMany)
    {
        foreach ($oneToMany as $pair) {
            $pass = $pair['pass'];

            $pass = (is_string($pass)) ? trim($pass) : '';
            $validPass = strlen($pass) > 0;

            foreach ($pair['file'] as $file) {
                $file = (is_string($file)) ? trim($file) : '';
                $validFile = strlen($file) > 0;

                if ($validFile && $validPass) {
                    $this->entries[] = [
                        'filename' => $file,
                        'password' => $pass,
                        'realname' => true
                    ];
                }
            }
        };
    }

    private function encFile(string $s)
    {
        $filekey = (is_string(session('filekey'))) ? session('filekey') : request('filekey');
        $key = base64_encode($filekey);

        // $fromKey = base64_decode($key);
        $toKey = base64_decode($key);
        $cipher = "AES-256-CBC"; //or AES-128-CBC if you prefer

        //Create two encrypters using different keys for each
        // $encrypterFrom = new Encrypter($fromKey, $cipher);
        $encrypterTo = new Encrypter($toKey, $cipher);

        // //Decrypt a string that was encrypted using the "from" key
        // $decryptedFromString = $encrypterFrom->decryptString($encrypterTo->encryptString($s));

        //Now encrypt the decrypted string using the "to" key
        return $encrypterTo->encryptString($s);
    }

    private function decFile(string $s)
    {
        $filekey = (is_string(session('filekey'))) ? session('filekey') : request('filekey');
        $key = base64_encode($filekey);

        $fromKey = base64_decode($key);
        // $toKey = base64_decode($key);
        $cipher = "AES-256-CBC"; //or AES-128-CBC if you prefer

        //Create two encrypters using different keys for each
        $encrypterFrom = new Encrypter($fromKey, $cipher);
        // $encrypterTo = new Encrypter($toKey, $cipher);

        // //Decrypt a string that was encrypted using the "from" key
        return $encrypterFrom->decryptString($s);
    }

    private function find($word)
    {
        $items = Entry::all()->filter(function($rec) use ($word) {
            if (strpos($this->decFile($rec->filename), $word) !== false) {
                return $rec;
            }
        });

        $results = [];

        foreach($items as $item) {
            $results[] = [
                'filename' => $this->decFile($item->filename),
                'password' => $this->decFile($item->password)
            ];
        }

        return $results;
    }

    public function findByFile()
    {
        return view('search', [
            'result' => $this->find(request('search'))
        ]);
    }

    public function getEntries()
    {
        return Entry::all()->filter(function($rec) {
            $rec->filename = $this->decFile($rec->filename);
            $rec->password = $this->decFile($rec->password);

            return $rec;
        });
    }
}
