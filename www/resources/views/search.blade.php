<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>Welcome!</title>

        <!-- Fonts -->
        <!-- <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet"> -->

        <!-- Styles -->
        <!-- <style></style> -->
    </head>
    <body>
        @foreach ($result as $item)
            <div>
                {{$item['filename']}}
                <input type="text" id="{{$item['filename']}}" value="{{$item['password']}}" readonly>
                <input type="button" value="Copy" onclick="copyText('{{$item['filename']}}')">
            </div>

        @endforeach
        <form id="find" action="/find" method="POST">
            @csrf
            <input type="text" name="search" placeholder="what?" id="searchbox">
            <input type="submit" value="Find">

            <input type="button" value="Paste & Search" onclick="pasteThenSearch()">
            <input type="button" value="Clear" onclick="document.getElementById('searchbox').value = '';">
        </form>
        <br><br>
        <input type="button" value="Back" onclick="location.href = '/';"><br><br>

        <script>
            function copyText(elementId) {
                copyText = document.getElementById(elementId);
                copyText.select();
                copyText.setSelectionRange(0, 99999); /*For mobile devices*/

                /* Copy the text inside the text field */
                document.execCommand("copy");
            }

            async function pasteThenSearch() {
                const clipboard = await navigator.clipboard.readText();
                document.getElementById('searchbox').value = clipboard;
                document.getElementById('find').submit();
            }
        </script>
    </body>
</html>
