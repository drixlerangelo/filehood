<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>Welcome!</title>

        <!-- Fonts -->
        <!-- <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet"> -->

        <!-- Styles -->
        <!-- <style></style> -->
    </head>
    <body>
        <form id="set_key" action="/keepkey" method="POST">
            @csrf
            <input type="hidden" name="key" id="key">
        </form>

        <script>
            let key = prompt('Please enter the key: ');

            if (key.length === 0) {
                location.reload();
            }

            document.getElementById('key').setAttribute('value', key);
            document.getElementById('set_key').submit();
        </script>
    </body>
</html>
