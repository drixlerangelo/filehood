<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>Welcome!</title>

        <!-- Fonts -->
        <!-- <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet"> -->

        <!-- Styles -->
        <!-- <style></style> -->
    </head>
    <body>
        <form id="logout" action="/logout" method="GET">
            @csrf
            <input type="submit" value="Logout">
        </form>
        <br><br>

        <form id="add_entry" method="POST" action="/entries/add">
            @csrf
            <table id="entries">
                <thead>
                    <tr>
                        <th>Files</th>
                        <th>Passwords</th>
                        <th>Remove</th>
                    </tr>
                </thead>
            </table>
        </form>

        <br><br>
        <input type="button" value="Add 1:1 entry" onclick="addOneToOneEntry()">
        <input type="button" value="Add 1:m entries" onclick="addOneToManyEntry()">
        <input type="button" value="Add Entries" onclick="document.getElementById('add_entry').submit()">
        <br><br>

        <form id="find" action="/find" method="POST">
            @csrf
            <input type="text" name="search" placeholder="what?" id="searchbox">
            <input type="submit" value="Find">
            <input type="button" value="Clear" onclick="document.getElementById('searchbox').value = '';">
        </form>
        <br><br>

        <script>
            let oneToOneCounter = 0,
                oneToManyCounter = 0,
                manyToOneCounter = 0,
                page = 0;
            getEntries();


            function addOneToOneEntry() {
                file_input = document.createElement('input');
                file_input.setAttribute('type', 'text');
                file_input.setAttribute('name', "oneToOne[file][]");
                file_input.setAttribute('placeholder', 'filename');

                file_td = document.createElement('td');
                file_td.appendChild(file_input);

                pass_input = document.createElement('input');
                pass_input.setAttribute('type', 'text');
                pass_input.setAttribute('name', "oneToOne[pass][]");
                pass_input.setAttribute('placeholder', 'pwd');

                pass_td = document.createElement('td');
                pass_td.appendChild(pass_input);

                remove_btn = document.createElement('input');
                remove_btn.setAttribute('type', 'button');
                remove_btn.setAttribute('onclick', 'removeEntry(this)');
                remove_btn.setAttribute('value', 'x');

                remove_td = document.createElement('td');
                remove_td.appendChild(remove_btn);

                entryRow = document.createElement('tr');
                entryRow.appendChild(file_td);
                entryRow.appendChild(pass_td);
                entryRow.appendChild(remove_td);

                document.getElementById('entries').appendChild(entryRow);

                return false;
            }

            function addOneToManyEntry() {
                items_div = document.createElement('div');
                items_div.setAttribute('id', 'one_to_many_item' + oneToManyCounter);

                add_item_btn = document.createElement('input');
                add_item_btn.setAttribute('type', 'button');
                add_item_btn.setAttribute('onclick', `addFile(${oneToManyCounter})`);
                add_item_btn.setAttribute('value', '+');

                files_td = document.createElement('td');
                files_td.appendChild(items_div);
                files_td.appendChild(add_item_btn);

                pass_input = document.createElement('input');
                pass_input.setAttribute('type', 'text');
                pass_input.setAttribute('name', `oneToMany[${oneToManyCounter}][pass]`);
                pass_input.setAttribute('placeholder', 'pwd');

                pass_td = document.createElement('td');
                pass_td.appendChild(pass_input);

                remove_btn = document.createElement('input');
                remove_btn.setAttribute('type', 'button');
                remove_btn.setAttribute('onclick', 'removeEntry(this)');
                remove_btn.setAttribute('value', 'x');

                remove_td = document.createElement('td');
                remove_td.appendChild(remove_btn);

                entryRow = document.createElement('tr');
                entryRow.appendChild(files_td);
                entryRow.appendChild(pass_td);
                entryRow.appendChild(remove_td);

                document.getElementById('entries').appendChild(entryRow);

                addFile(oneToManyCounter);
                oneToManyCounter += 1;

                return false;
            }

            function addFile(num) {
                file_input = document.createElement('input');
                file_input.setAttribute('type', 'text');
                file_input.setAttribute('name', `oneToMany[${num}][file][]`);
                file_input.setAttribute('placeholder', 'filename');

                rm_item_btn = document.createElement('input');
                rm_item_btn.setAttribute('type', 'button');
                rm_item_btn.setAttribute('onclick', 'removeItem(this)');
                rm_item_btn.setAttribute('value', 'x');

                item_div = document.createElement('div');
                item_div.appendChild(file_input);
                item_div.appendChild(rm_item_btn);

                document.getElementById('one_to_many_item' + num).appendChild(item_div);
            }

            function removeEntry(element) {
                let targetNode = element.parentNode.parentNode;
                targetNode.parentNode.removeChild(targetNode);
            }

            function removeItem(element) {
                let targetNode = element.parentNode;
                targetNode.parentNode.removeChild(targetNode);
            }

            function getEntries() {
                var xmlHttp = new XMLHttpRequest();

                xmlHttp.open('GET', '/entries/all?page=' + page, false);
                xmlHttp.send();

                console.log(JSON.parse(xmlHttp.responseText));
            }
        </script>
    </body>
</html>
