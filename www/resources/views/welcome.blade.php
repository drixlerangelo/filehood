<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>Welcome!</title>

        <!-- Fonts -->
        <!-- <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet"> -->

        <!-- Styles -->
        <!-- <style></style> -->
    </head>
    <body>
        <form id="login" action="/login" method="POST">
            @csrf
            Username: <input type="text" name="username" id="username"><br>
            Password: <input type="password" name="password" id="password"><br><br>
            <input type="button" onclick="login()" value="Submit form">
        </form>

        <script>
            document.addEventListener('DOMContentLoaded', function(){
                document.getElementById('password').onkeypress = function (event) {
                    if (event.keyCode === 13) {
                        document.getElementById('login').submit();
                    }
                };
            }, false );

            function login() {
                document.getElementById('login').submit();
            }
        </script>
    </body>
</html>
