<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'UserController@index')->name('index');
Route::post('/login', 'UserController@login')->name('login');
Route::post('/entries/anonymouse', 'EntryController@anonymouse')->name('anonymouse');
Route::post('/find', 'EntryController@findByFile')->name('find');

Route::middleware(['chklogin', 'auth'])->group(function () {
    Route::get('/middleman', 'UserController@middleman')->name('middleman');
    Route::post('/keepkey', 'UserController@keepkey')->name('keepkey');
    Route::get('/logout', 'UserController@logout')->name('logout');

    Route::post('/entries/add', 'EntryController@place')->name('insert');
    Route::get('/entries/all', 'EntryController@getEntries')->name('all');
});
